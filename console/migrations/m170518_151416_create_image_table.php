<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image`.
 */
class m170518_151416_create_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'image' => $this->string(),
        ]);
        $this->createIndex(
            'idx-image-user_id',
            'image',
            'user_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('image');
    }
}
