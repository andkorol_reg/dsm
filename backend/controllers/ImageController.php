<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Image;
use yii\web\UploadedFile;

/**
 * Image controller
 */
class ImageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays Index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Image();

        if (Yii::$app->request->isPost) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->upload()) {
                // save new record
                $model->user_id = (int)Yii::$app->user->id;
                $model->image = $model->fileName;
                unset($model->fileName);
                $model->save();
            }
            return $this->redirect(['index']);
        }
        // get all user records
        $images = $model->find()->where(['user_id' => Yii::$app->user->id])->orderBy('id desc')->all();
        return $this->render('index', compact('model', 'images'));
    }

    /**
     * Delete image from DB and from uploads directory
     */
    public function actionDelete()
    {
        if (Yii::$app->request->isPost) {
            $id = (int)Yii::$app->request->post('id'); // DB record ID
            $image = Image::findOne($id);
            if(!empty($image)){
                unlink(Yii::getAlias('@frontend') . '/web/uploads/' . $image->image); // delete from directory
                $image->delete(); // delete from DB
                echo 1;
                return;
            }
        }
        echo 0;
    }
}
