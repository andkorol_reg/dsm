function rotateImage(id){
    var angle = ($('#img_'+id).data('angle') + 90) || 90;
    $('#img_'+id).css({'transform': 'rotate(' + angle + 'deg)'});
    $('#img_'+id).data('angle', angle);
}

function deleteImage(id){
    $.post(
        '/image/delete',
        { id : id },
        function(data){
            if(data*1 == 1){
                $('#tr_'+id).remove();
            }
        },
        'text'
    );
}