<?php

/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'My Images';
?>

<div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?= $form->field($model, 'image')->fileInput(); ?>
        <button class="btn btn-success">Upload</button>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php if(!empty($images)): ?>
<div class="row images-table-block">
    <div class="col-md-8">
        <table class="table table-bordered table-striped">
            <tbody>
            <?php foreach($images as $row): ?>
            <tr id="tr_<?= $row['id']; ?>">
                <td class="centered">
                    <?= Html::img(
                        Yii::$app->params['urlUploads'] . $row['image'],
                        ['id' => 'img_' . $row['id'], 'class' => 'img-prev']
                    ); ?>
                </td>
                <td class="centered">
                    <button class="btn btn-primary" onclick="rotateImage(<?= $row['id']; ?>);">Rotate</button>
                </td>
                <td class="centered">
                    <button class="btn btn-danger" onclick="deleteImage(<?= $row['id']; ?>);">Delete</button>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif; ?>