<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'domain' => 'dsm.loc',
    'adminDomain' => 'admin.dsm.loc',
    'urlFrontend' => 'http://dsm.loc/',
    'urlBackend' => 'http://admin.dsm.loc/',
    'urlUploads' => 'http://dsm.loc/uploads/',
];
