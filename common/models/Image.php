<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $image
 */
class Image extends \yii\db\ActiveRecord
{
    public $fileName;
    /**
     * Model table name
     * @return string
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * Validation rules
     * @return array
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'png, jpg, gif, jpeg'],
        ];
    }

    /**
     * Attribute labels
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'image' => 'Image:',
        ];
    }

    /**
     * File uploading
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $this->fileName = md5($this->image->baseName . time()) . '.' . $this->image->extension;
            $this->image->saveAs(Yii::getAlias('@frontend') . '/web/uploads/' . $this->fileName);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Image owner
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
