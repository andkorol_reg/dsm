<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Image;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Image();
        $images = $model->find()->with('user')->orderBy('id desc')->all();
        if(!empty($images)) shuffle($images);
        return $this->render('index', compact('images'));
    }


}
