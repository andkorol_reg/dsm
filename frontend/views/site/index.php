<?php

/* @var $this yii\web\View */

$this->title = 'Gallery';
?>

<div class="row">
<?php if(!empty($images)): ?>
    <?php foreach($images as $image): ?>
    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2">
        <div class="thumbnail">
            <?= \yii\helpers\Html::img(
                Yii::$app->params['urlUploads'] . $image['image'],
                ['alt' => 'Image #' . $image['id'], 'class' => 'img-responsive gallery-img']
            ); ?>
            <div class="caption">
                <h4 class="text-center"><?= $image->user->username; ?></h4>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="col-md-12">
        <h3>No images((</h3>
    </div>
<?php endif; ?>
</div>